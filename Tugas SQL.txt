Tugas SQL

1. Membuat Database di Sql
Create database myshop

2.Membuat Tabel di Dalam Database
   Sebelum membuat tabel, membuat syntax dengan command: use myshop; 
   maka akan muncul command = MariaDB [myshop]
a. Membuat tabel Users:
   create table users(id int AUTO_INCREMENT, name varchar(255), email varchar(255), password varchar(255), PRIMARY KEY (id));
b. Membuat tabel Categories:
   create table categories(id int AUTO_INCREMENT, name varchar(255), PRIMARY KEY (id));
c. Membuat Tabel Items:
   create table items(id int AUTO_INCREMENT, name varchar(255), description varchar(255), price int, stock int, categories_id int, PRIMARY KEY (id), FOREIGN KEY (categories_id) REFERENCES categories(id));

3.Memasukan Data Tabel
a. Tabel User: 
   insert into users values ('','John Doe', 'john@doe.com', 'john123'), ('','Jane Doe', 'jane@doe.com', 'jenita123');
b. Tabel Categories:
c. Tabel Items:
insert into items values ('','Sumsang b50','hape keren dari merek sumsang',4000000,100,1), ('', 'Uniklooh', 'baju keren dari brand ternama',500000,50,2),('','IMHO Watch','jam tangan anak yang jujur banget',2000000,10,1);

4. Mengambil Data User
a. Mengambil data users:
   select id, name, email from users;
b. Mengambil data items:
select * from items where price > 1000000;
select * from items where name LIKE '%sang%';
c. Menampilkan data items join dengan kategori
SELECT items.name, items.description, items.price, items.stock, items.categories_id, categories.name FROM items INNER JOIN categories ON items.categories_id=categories.id;

5. Mengubah Data Dari Database
UPDATE items SET price=2500000 where name='sumsang b50';